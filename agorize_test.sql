--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

-- Started on 2019-05-19 20:10:55 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE agorize_test;
--
-- TOC entry 3147 (class 1262 OID 16393)
-- Name: agorize_test; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE agorize_test WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


\connect agorize_test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16394)
-- Name: skills; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.skills (
    id integer NOT NULL,
    name character varying,
    parent_id integer
);


--
-- TOC entry 198 (class 1259 OID 16412)
-- Name: skills_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.skills_users (
    id integer NOT NULL,
    skill_id integer,
    user_id integer
);


--
-- TOC entry 197 (class 1259 OID 16407)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    points integer
);


--
-- TOC entry 3139 (class 0 OID 16394)
-- Dependencies: 196
-- Data for Name: skills; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.skills (id, name, parent_id) VALUES (1, 'Football', NULL);
INSERT INTO public.skills (id, name, parent_id) VALUES (2, 'Basketball', NULL);
INSERT INTO public.skills (id, name, parent_id) VALUES (3, 'Foot', 1);
INSERT INTO public.skills (id, name, parent_id) VALUES (4, 'Basket', 2);
INSERT INTO public.skills (id, name, parent_id) VALUES (5, 'Soccer', 1);


--
-- TOC entry 3141 (class 0 OID 16412)
-- Dependencies: 198
-- Data for Name: skills_users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.skills_users (id, skill_id, user_id) VALUES (1, 1, 1);
INSERT INTO public.skills_users (id, skill_id, user_id) VALUES (2, 1, 2);
INSERT INTO public.skills_users (id, skill_id, user_id) VALUES (3, 3, 3);
INSERT INTO public.skills_users (id, skill_id, user_id) VALUES (4, 2, 4);
INSERT INTO public.skills_users (id, skill_id, user_id) VALUES (5, 5, 5);


--
-- TOC entry 3140 (class 0 OID 16407)
-- Dependencies: 197
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.users (id, points) VALUES (1, 100);
INSERT INTO public.users (id, points) VALUES (2, 200);
INSERT INTO public.users (id, points) VALUES (3, 100);
INSERT INTO public.users (id, points) VALUES (4, 50);
INSERT INTO public.users (id, points) VALUES (5, 10);


--
-- TOC entry 3010 (class 2606 OID 16401)
-- Name: skills skills_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (id);


--
-- TOC entry 3014 (class 2606 OID 16416)
-- Name: skills_users skills_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.skills_users
    ADD CONSTRAINT skills_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3012 (class 2606 OID 16411)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3015 (class 2606 OID 16402)
-- Name: skills skills_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.skills
    ADD CONSTRAINT skills_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.skills(id);


--
-- TOC entry 3016 (class 2606 OID 16417)
-- Name: skills_users skills_users_skills_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.skills_users
    ADD CONSTRAINT skills_users_skills_id_fkey FOREIGN KEY (skill_id) REFERENCES public.skills(id);


--
-- TOC entry 3017 (class 2606 OID 16422)
-- Name: skills_users skills_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.skills_users
    ADD CONSTRAINT skills_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


-- Completed on 2019-05-19 20:10:55 CEST

--
-- PostgreSQL database dump complete
--

