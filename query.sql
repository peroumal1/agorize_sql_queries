with recursive skills_score(id,name,points,users) as (
    select s.id, s.name, u.points, u.id from skills s, users u, skills_users su
    where s.parent_id is null and u.id = su.user_id and s.id = su.skill_id
    union 
    select skills_score.id, skills_score.name, u2.points, u2.id from skills s2, users u2, skills_users su2, skills_score
    where s2.parent_id = skills_score.id and u2.id = su2.user_id and s2.id = su2.skill_id
) select id,name,sum(points) as points,count(users) as users_count from skills_score group by id,name order by id;
