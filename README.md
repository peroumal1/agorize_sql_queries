Environment used: Postgres 11 on Mac OS 10.14.5

To deal with the first question, I created tables and data as close as possible as what was displayed in the exercise.

There are two files in this repository:

* . agorize_test.sql: the dump from the database with data
*  query.sql: the query requested to display the total of points and number of users for the main skills (without parent)

